
Wiring List for Kria adapter
Based on Mercury connectors

Dir refers to SoM

#+NAME: M-CONN-A
| M-pin | M-name      | SM Net               | Dir | M-VIO | K-VIO | K-pin | K-name          | Notes                        |
|-------|-------------|----------------------|-----|-------|-------|-------|-----------------|------------------------------|
| 1     | VIN_MOD     | 12V                  | In  |       |       |       | to 5V via DC/DC |                              |
| 2     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 3     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 4     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 5     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 6     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 7     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 8     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 9     | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 10    | PWR_EN      | ZYNQ_EN              | In  | 3.3   | 5.0   | 1-B26 | PWR_OFF_C2M_L   | Act-L 5V PU, need FET?       |
| 11    | VIN_MOD     | 12V                  |     |       |       |       |                 |                              |
| 12    | PWR_GOOD    | ZYNQ_PWR_GOOD        |     | 3.3   |       |       |                 | Possibly multiple Kria lines |
| 13    | GND         |                      |     |       |       |       |                 |                              |
| 14    | GND         |                      |     |       |       |       |                 |                              |
| 15    | IO_3.3V     | Mezz1_UART_Tx        | Out | 3.3   |       |       |                 |                              |
| 16    | IO_3.3V     | Mezz1_GPIO1          |     | 3.3   |       |       |                 |                              |
| 17    | IO_3.3V     | Mezz1_UART_Rx        | In  | 3.3   |       |       |                 |                              |
| 18    | IO_3.3V     | Mezz1_GPIO2          |     | 3.3   |       |       |                 |                              |
| 19    | IO_3.3V     | Mezz2_UART_Tx        | Out | 3.3   |       |       |                 |                              |
| 20    | GND         |                      |     |       |       |       |                 |                              |
| 21    | IO_3.3V     | Mezz2_UART_Rx        | In  | 3.3   |       |       |                 |                              |
| 22    | IO_3.3V     | LED_SCL              | I/O | 3.3   |       |       |                 |                              |
| 23    | GND         |                      |     |       |       |       |                 |                              |
| 24    | IO_3.3V     | LED_RST              | Out | 3.3   |       |       |                 |                              |
| 25    | IO_3.3V     | LED_SDA              | I/O | 3.3   |       |       |                 |                              |
| 26    | VCC_3V3_OUT |                      |     |       |       |       |                 |                              |
| 27    | IO_3.3V     | SENSE_Zynq_SDA       | I/O | 3.3   |       |       |                 |                              |
| 28    | IO_3.3V     | SENSE_Zynq_SCL       | I/O | 3.3   |       |       |                 |                              |
| 29    | VCC_3V3_OUT |                      |     |       |       |       |                 |                              |
| 30    | IO_3.3V     | SWITCHLED_SCK        | I/O | 3.3   |       |       |                 |                              |
| 31    | IO_3.3V     | SWITCH_LED_SDA       | I/O | 3.3   |       |       |                 |                              |
| 32    | GND         |                      |     |       |       |       |                 |                              |
| 33    | IO_3.3V     | ZYNQ_M2_PS_RST       | Out | 3.3   |       |       |                 |                              |
| 34    | IO_3.3V     | LHC_CLK_SEL          | Out | 3.3   |       |       |                 |                              |
| 35    | GND         |                      |     |       |       |       |                 |                              |
| 36    | IO_3.3V     | LHC_CLK_BP_LOS       | In  | 3.3   |       |       |                 |                              |
| 37    | IO_3.3V     | LHC_CLK_OSC_LOS      | In  | 3.3   |       |       |                 |                              |
| 38    | VCC_IO_BN   | 3.3V                 |     |       |       |       |                 |                              |
| 39    | IO_3.3V     | HQ_CLK_SEL           | Out | 3.3   |       |       |                 |                              |
| 40    | IO_3.3V     | HQ_CLK_BP_LOS        | In  | 3.3   |       |       |                 |                              |
| 41    | VCC_IO_B0   | 3.3V                 |     |       |       |       |                 |                              |
| 42    | IO_3.3V     | HQ_CLK_OSC_LOS       | In  | 3.3   |       |       |                 |                              |
| 43    | IO_3.3V     | Mezz2_EN             | Out | 3.3   |       |       |                 |                              |
| 44    | GND         |                      |     |       |       |       |                 |                              |
| 45    | IO_3.3V     | Mezz1_EN             | Out | 3.3   |       |       |                 |                              |
| 46    | IO_3.3V     | EEPROM_WE_N          | Out | 3.3   |       |       |                 |                              |
| 47    | GND         |                      |     |       |       |       |                 |                              |
| 48    | IO_3.3V     | ZYNQ_CPLD_GPIO1      | ?   | 3.3   |       |       |                 |                              |
| 49    | IO_3.3V     | ZYNQ_CPLD_GPIO2      | ?   | 3.3   |       |       |                 |                              |
| 50    | VCC_3V3_OUT |                      |     |       |       |       |                 |                              |
| 51    | IO_3.3V     | ZYNQ_CPLD_GPIO3      | ?   | 3.3   |       |       |                 |                              |
| 52    | IO_3.3V     | UART_Rx_Zynq_PL      |     |       |       |       |                 |                              |
| 53    | VCC_2V5_OUT |                      |     |       |       |       |                 |                              |
| 54    | IO_3.3V     | UART_Tx_Zynq_PL      |     |       |       |       |                 |                              |
| 55    | IO_3.3V     | ZYNQ_M1_TCK          |     |       |       |       |                 |                              |
| 56    | GND         |                      |     |       |       |       |                 |                              |
| 57    | IO_3.3V     | ZYNQ_M1_TMS          |     |       |       |       |                 |                              |
| 58    | IO_3.3V     | ZYNQ_M1_TDO          |     |       |       |       |                 |                              |
| 59    | GND         |                      |     |       |       |       |                 |                              |
| 60    | IO_3.3V     | ZYNQ_M1_TDI          |     |       |       |       |                 |                              |
| 61    | IO_3.3V     |                      |     |       |       |       |                 |                              |
| 62    | VCC_2V5_OUT |                      |     |       |       |       |                 |                              |
| 63    | IO_3.3V     | Mezz1_Mon_Rx         |     |       |       |       |                 |                              |
| 64    | IO_3.3V     | Mezz1_GPIO0          |     |       |       |       |                 |                              |
| 65    | VCC_2V5_OUT |                      |     |       |       |       |                 |                              |
| 66    | IO_3.3V     | Mezz2_PWR_EN         |     |       |       |       |                 |                              |
| 67    | IO_3.3V     | Mezz2_Mon_Rx         |     |       |       |       |                 |                              |
| 68    | GND         |                      |     |       |       |       |                 |                              |
| 69    | IO_3.3V     | Mezz2_GPIO0          |     |       |       |       |                 |                              |
| 70    | IO_3.3V     | ZYNQ_M1_PS_RST       |     |       |       |       |                 |                              |
| 71    | GND         |                      |     |       |       |       |                 |                              |
| 72    | IO_3.3V     | FP_BUTTON            |     |       |       |       |                 |                              |
| 73    | IO_3.3V     | BOOT_SEQ_DONE        |     |       |       |       |                 |                              |
| 74    | VCC_CFG_MIO | 3.3V                 |     |       |       |       |                 |                              |
| 75    | IO_3.3V     | SWITCH_UART_Rx_local |     |       |       |       |                 |                              |
| 76    | IO_3.3V     | SWITCH_UART_Tx_local |     |       |       |       |                 |                              |
| 77    | VCC_CFG_MIO | 3.3V                 |     |       |       |       |                 |                              |
| 78    | IO_3.3V     |                      |     |       |       |       |                 |                              |
| 79    | IO_3.3V     |                      |     |       |       |       |                 |                              |
| 80    | GND         |                      |     |       |       |       |                 |                              |
| 81    | IO_3.3V     |                      |     |       |       |       |                 |                              |
| 82    | PS_MIO40    |                      |     |       |       |       |                 |                              |
| 83    | GND         |                      |     |       |       |       |                 |                              |
| 84    | PS_MIO41    | GPIO0                |     |       |       |       |                 |                              |
| 85    | IO_3.3V     |                      |     |       |       |       |                 |                              |
| 86    | VCC_3V3_OUT |                      |     |       |       |       |                 |                              |
| 87    | IO_3.3V     |                      |     |       |       |       |                 |                              |
| 88    | IO_B64      | Mezz1_PWR_GOOD       |     |       |       |       |                 |                              |
| 89    | VCC_2V5_OUT |                      |     |       |       |       |                 |                              |
| 90    | IO_B64      | Mezz2_PWR_GOOD       |     |       |       |       |                 |                              |
| 91    | MIO_SDCLK   | Zynq_SDCARD_CLK      |     |       |       |       |                 |                              |
| 92    | IO_B64      | ZYNQ_CPLD_GPIO0      |     |       |       |       |                 |                              |
| 93    | MIO_SDCMD   | Zynq_SDCARD_CMD      |     |       |       |       |                 |                              |
| 94    | IO_B64      | ZYNQ_M2_TDI          |     |       |       |       |                 |                              |
| 95    | MIO_SDD0    | Zynq_SDCARD_D0       |     |       |       |       |                 |                              |
| 96    | GND         |                      |     |       |       |       |                 |                              |
| 97    | MIO_SDD1    | Zynq_SDCARD_D1       |     |       |       |       |                 |                              |
| 98    | PS_MIO45    | ZRevID0              |     |       |       |       |                 |                              |
| 99    | GND         |                      |     |       |       |       |                 |                              |
| 100   | PS_MIO44    | ZRevID1              |     |       |       |       |                 |                              |
| 101   | MIO_SDD2    | Zynq_SDCARD_D2       |     |       |       |       |                 |                              |
| 102   | VMON_INT    |                      |     |       |       |       |                 |                              |
| 103   | MIO_SDD3    | Zynq_SDCARD_D3       |     |       |       |       |                 |                              |
| 104   | PS_MIO42    | ZRevID0              |     |       |       |       |                 |                              |
| 105   | MIO_UARTRX  | UART_Rx_Zynq_PS      |     |       |       |       |                 |                              |
| 106   | PS_MIO43    |                      |     |       |       |       |                 |                              |
| 107   | MIO_UARTTX  | UART_Tx_Zynq_PS      |     |       |       |       |                 |                              |
| 108   | GND         |                      |     |       |       |       |                 |                              |
| 109   | GND         |                      |     |       |       |       |                 |                              |
| 110   | VBUS_DETECT |                      |     |       |       |       |                 |                              |
| 111   | I2C_SCL     | PWR_IPMC_SCL         |     |       |       |       |                 |                              |
| 112   | BOOT_MODE1  | ZYNQ_MODE1           |     |       |       |       |                 |                              |
| 113   | I2C_SDA     | PWR_IPMC_SDA         |     |       |       |       |                 |                              |
| 114   | FLASH_DI    |                      |     |       |       |       |                 |                              |
| 115   | I2C_INT#    |                      |     |       |       |       |                 |                              |
| 116   | FLASH_CS#   |                      |     |       |       |       |                 |                              |
| 117   | JTAG_TDI    |                      |     |       |       |       |                 |                              |
| 118   | FLASH_CLK   |                      |     |       |       |       |                 |                              |
| 119   | JTAG_TMS    | JTAG_TMS             |     |       |       |       |                 |                              |
| 120   | GND         |                      |     |       |       |       |                 |                              |
| 121   | JTAG_TDO    | JTAG_TDO             |     |       |       |       |                 |                              |
| 122   | FLASH_D0    |                      |     |       |       |       |                 |                              |
| 123   | JTAG_TCK    | JTAG_TCK             |     |       |       |       |                 |                              |
| 124   | PS_SRST#    | JTAG_PS_RST          |     |       |       |       |                 |                              |
| 125   | GND         |                      |     |       |       |       |                 |                              |
| 126   | BOOT_MODE0  | ZYNQ_MODE0           |     |       |       |       |                 |                              |
| 127   | (USB)       |                      |     |       |       |       |                 |                              |
| 128   | (USB)       |                      |     |       |       |       |                 |                              |
| 129   | (USB)       |                      |     |       |       |       |                 |                              |
| 130   | PS_DONE     | ZYNQ_DONE            |     |       |       |       |                 |                              |
| 131   | (USB)       |                      |     |       |       |       |                 |                              |
| 132   | PS_POR#     | PW_POR_N             |     |       |       |       |                 |                              |
| 133   | (USB)       |                      |     |       |       |       |                 |                              |
| 134   | GND         |                      |     |       |       |       |                 |                              |
| 135   | GND         |                      |     |       |       |       |                 |                              |
| 136   |             |                      |     |       |       |       |                 |                              |
| 137   |             |                      |     |       |       |       |                 |                              |
| 138   | GND         |                      |     |       |       |       |                 |                              |
| 139   | GND         |                      |     |       |       |       |                 |                              |
| 140   |             |                      |     |       |       |       |                 |                              |
| 141   |             |                      |     |       |       |       |                 |                              |
| 142   | GND         |                      |     |       |       |       |                 |                              |
| 143   | GND         |                      |     |       |       |       |                 |                              |
| 144   | ETH1_D_N    | ETH1_D_N             |     |       |       |       |                 |                              |
| 145   | ETH0_D_N    | ETH0_D_N             |     |       |       |       |                 |                              |
| 146   | ETH1_D_P    | ETH1_D_P             |     |       |       |       |                 |                              |
| 147   | ETH0_D_P    | ETH0_D_P             |     |       |       |       |                 |                              |
| 148   | ETH1_LED1#  | ETH1_LED0            |     |       |       |       |                 |                              |
| 149   | ETH0_LED1#  | ETH0_LED0            |     |       |       |       |                 |                              |
| 150   | ETH1_C_N    | ETH1_C_N             |     |       |       |       |                 |                              |
| 151   | ETH0_C_N    | ETH0_C_N             |     |       |       |       |                 |                              |
| 152   | ETH1_C_P    | ETH1_C_P             |     |       |       |       |                 |                              |
| 153   | ETH0_C_P    | ETH0_C_P             |     |       |       |       |                 |                              |
| 154   | ETH1_CTREF  | ETH1_CTREF           |     |       |       |       |                 |                              |
| 155   | ETH0_CTREF  | ETH0_CTREF           |     |       |       |       |                 |                              |
| 156   | ETH1_B_N    | ETH1_B_N             |     |       |       |       |                 |                              |
| 157   | ETH0_B_N    | ETH0_B_N             |     |       |       |       |                 |                              |
| 158   | ETH1_B_P    | ETH1_B_P             |     |       |       |       |                 |                              |
| 159   | ETH0_B_P    | ETH0_B_P             |     |       |       |       |                 |                              |
| 160   | ETH1_LED2#  | ETH1_LED1            |     |       |       |       |                 |                              |
| 161   | ETH0_LED2#  | ETH0_LED1            |     |       |       |       |                 |                              |
| 162   | ETH1_A_N    | ETH1_A_N             |     |       |       |       |                 |                              |
| 163   | ETH0_A_N    | ETH0_A_N             |     |       |       |       |                 |                              |
| 164   | ETH1_A_P    | ETH1_A_P             |     |       |       |       |                 |                              |
| 165   | ETH0_A_P    | ETH0_A_P             |     |       |       |       |                 |                              |
| 166   | GND         |                      |     |       |       |       |                 |                              |
| 167   | GND         |                      |     |       |       |       |                 |                              |
| 168   |             |                      |     |       |       |       |                 |                              |
