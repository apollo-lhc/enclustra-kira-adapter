(require 'org)
;;
;; export all org-mode tables to CSV files
;; N.B. no over-write checking
;; Searches for "#NAME: my_table_name
;; (table name must be 
;; Exports following table to my_table_name.csv
;;
;; Run in batch mode as:
;; emacs --batch my_data.org -l export-tables.el --eval '(export-tables)'
;;
(defun next-tbl-export ()
  "Search for next table and export to named CSV file."
  (show-all)
  (let ((case-fold-search t))
    (if (search-forward-regexp "#\\+NAME: \\([[:print:]]+\\)" nil t)
    (progn
      (setq name (match-string 1))
      (next-line)
      (org-table-export (format "%s.csv" name) "orgtbl-to-csv")
      ))))

(defun export-tables ()
  "Export all named org-mode tables in buffer."
  (while (next-tbl-export)
    (print (concat "Exported a table: " name))))
