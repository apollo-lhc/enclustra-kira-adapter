#!/usr/bin/perl
#   read a connector list and print as org-mode table

use strict;
use Text::CSV;
use Data::Dumper;

my $org = 0;

my $csv = Text::CSV->new( { binary => 1, auto_diag => 1});
open my $fh, "<", $ARGV[0] or die "Opening $ARGV[0]";

if( !$org) {
    print qq{"M-pin","M-name","SM Net","Dir","M-VIO","K-VIO","K-pin","K-name","Notes"\n};
}

while( my $row = $csv->getline( $fh)) {
    # expect net, name, number, number, name, net
    if( $org) {			# output org-mode table
	printf "| %s | %s | %s |\n", $row->[2], $row->[1], $row->[0];
	printf "| %s | %s | %s |\n", $row->[3], $row->[4], $row->[5];
    } else {			# output CSV
	printf qq{%s,"%s","%s",,,,,,\n}, $row->[2], $row->[1], $row->[0];
	printf qq{%s,"%s","%s",,,,,,\n}, $row->[3], $row->[4], $row->[5];
    }
}
