
e = 0.1;
pcbw = 97.8;
pcbh = 77.6;
pcbt = 1.6;

module sfp() {
     translate( [0, 6.5, 0])
     import( "SFP-2227023-1.stl", convexity=3);
}

module adm6() {
     translate( [0, 0, pcbt+0.4])
     rotate( [90, 0, 0])
	  color("violet")
	  import( "ADM6-60-01.5-L-4-2-TR.stl", convexity=3);
}

module fx10() {
     translate( [2.25, -6.5, -pcbt/2])          
     rotate( [-90, 0, 0])
     color( [1.0, 0.5, 0.5])
	  import( "Unnamed-FX10A-168S-SV.stl", convexity=3);
}

module kria() {
     translate( [77-(77-70.65)/2, (60-53.65)/2, 6.9]) {
	  color( [0.5, 0.5, 1.0, 0.5])
	       import( "Unnamed-Kria_K26.stl", convexity=3);
     }
}

module pcb() {
     color( [0.2, 0.7, 0.2])
     difference() {
	  cube( [pcbw, pcbh, pcbt]);
	  translate( [pcbw-26.8, pcbh-11.1, -e])
	       cube( [27, 12, pcbt+2*e]);
	  translate( [-e, -e, -e])
	       cube( [e+15.5, e+20.5, pcbt+2*e]);
     }
}
	  

translate( [18, 69, 0]) rotate( [90, 0, -90]) sfp();

% translate( [95.5-77, 0, 0]) kria();
pcb();

// bottom connectors
translate( [58.5, 6, 0])  rotate( [0, 0, 90]) fx10();
translate( [58.5, 48, 0]) rotate( [0, 0, 90]) fx10();
translate( [91.5, 27, 0]) rotate( [0, 0, 0]) fx10();

/* top connectors */
translate( [23.7, 30, 0]) rotate( [0, 0, 90]) adm6();
translate( [23.7+66.612, 30, 0]) rotate( [0, 0, 90]) adm6();

