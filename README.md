# Enclustra Kria Adapter

The Apollo service module has historically used Enclustra Mercury+
series SoCs.  As of now (2022) these are almost unobtainable.  Others
are switching to the Xilinx "Kira" module.

This page describes a possible adapter to allow a Kira to be used on
an Apollo SM via the Enclustra connectors.  The sketch below shows the
approximate outline of a Kira module superimposed on the SM SoC site.
The corner intrudes a bit on a space reserved for something which is
not installed on any of our ATCA shelves, so it should be OK for
testing.

![footprint sketch](footprint_sketch.jpg?)

The stack height works OK too with no heatsink, so for brief tests
without a lot of power dissipation in the SoC we should be able to
mount an adapter/Kira stack on a blade and put it in a shelf.

## Problems

A big one is that the MIO banks are 3.3V on the SM and 1.8V on the
Kria.  This covers the following pin groups:

* JTAG - driven by CPLD on SM.  Could put a level shifter on adapter
* uSD card - problematic as data is bidirectional.  Maybe best to just
  put our own uSD card slot on the adapter
* ZYNQ_DONE - need level shifter
* PS_RST - can be always high per Xilinx
* PS_POR_B - need level shifter
* BOOT_MODE1/0 - on-board logic with level shifter needed
* IPMC, UART - move to 3.3V bank on Kria

Another is that the Kria has no Ethernet PHYs.  So we probably need to
put two of them on the adapter board.  ETH0 just goes to a magjack on
the SM; probably best to put the Magjack on the adapter for
stand-alone use.  ETH1 goes to the ESM and would need a PHY too.

A crazy option would be to just wire up an SFP and use an RJ-45 PHY.

See google spreadsheet for pin list.

## Parts

Level shifters in stock (first few listed by stock qty on DK):

* 74AVCH2T45GT,115 (sgl bidirectional with control pin, 8-XSON 1.95x1mm)
* SN74AXC1T45QDCKRQ1 (sgl bidirectional with control pin, 6-TSSOP)
* 74AVC1T45GS-Q100H (sgl bidirectional with control pin, 6-XSON 1x1mm)
* NLSV4T244MUTAG (4 bit uni 12-UQFN 1.7x2mm)
* FXLP34L6X (sgl uni 5-UFDFN)
* SN74AVC2T245RSWR (sgl bidirectional with control pin and OE, 10-UQFN 1.8x1.4)
